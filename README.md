# Scripts for MMPOS

With the addition of the API, orchestration across an entire farm is much easier.  These scripts
will help fill in some of the missing pieces when using ssh as your orchestration engine.

## Requirements

1. Ansible
2. MMPOS API Key
3. Access to a linux system or one of your rigs
4. Git command line

## Copying ssh keys

In order to use ansible and other ssh commands you need to put your trusted ssh public key on all the systems you wish to manage.  This can be done via the distribute_ssh_keys.sh script.  Run this either from a mmpos system or other linux system that you use as a central management system.  If you do not have a ssh key, the script will create one for you.

At the moment you will need to enter your miner password once for every machine you have.  Suggest copy-n-paste

## Ansible Inventory

The ansible inventory script will pull all the IPs out of the MMPOS API.  These IPs are then given to ansible for use with orchestration and mass upgrades across the farm.  You will need to edit the sample_mmpos_config.yml file with your API key and then move it to `~/.config/mmpos_config.yml`

### Example Ansible Commands

I'll preface that with any ansible command always make sure ansible is working on all hosts first by running a test command.
`ansible all -i mmpos_ansible_inventory.py -m uptime`

Before running ansible with the inventory script the first time make sure you have distributed your ssh keys to all your rigs.

* `ansible all -i mmpos_ansible_inventory.py -m shell -a "uptime"`
* `ansible all -i mmpos_ansible_inventory.py -m shell -a "mmp update -y"`
* `ansible all -i mmpos_ansible_inventory.py -m shell -a "agent-stop"`
* `ansible all -i mmpos_ansible_inventory.py -m shell -a "mmp-install-crossbelt.sh -y"`

## Usage

1. `git clone https://gitlab.com/blockops/mmpos-scripts.git`
2. `cd mmpos-scripts`
3. `./distribute_ssh_keys.sh`
4. `cp sample_mmpos_config.yml ~/.config/mmpos_config.yml`  # edit these values
4. `ansible all -i mmpos_ansible_inventory.py -m shell -a "uptime"`

### Custom Control Action script

Mainly written for myself you can use these scripts provided you pass your own token.  This script will run the same action on all your rigs from the first farm id found.  

`bash ./run_action_on_all.sh <action> <token>`

Please remember you must supply your token, this one is example only

ie. `./run_action_on_all.sh poweroff 39232-2323232-232323232`
ie. `./run_action_on_all.sh disable 39232-2323232-232323232`

You can also set an environment variable `MMPOS_API_TOKEN` for passing the token.

```shell
export MMPOS_API_TOKEN=39232-2323232-232323232 # set in .profile, .zshrc or other
./run_action_on_all.sh disable

```

### Python CLI tool

I have started to mess around with a python based cli tool that calls a few MMPOS API endpoints.  Very experimental and build for my usage.  You can try it out but you need to set the environment variable MMPOS_API_TOKEN first.  The cli --help should detail everything else needed.

You may need to install a few python libraries.  Notibly click, prettytable and possibly requests.  `pip3 install click`

`mmpos-cli.py --help`

More to come...

## Notes

* If you want to run the mmpos_ansible_inventory.py script directly you will need to install the requests library first.  `pip3 install requests`

* When generating a new ssh key pair you can opt out of the password entry.  While it is best to have a password you would also need to run an ssh-agent to handle password entry which is not covered at this time.

```
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/miner/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/miner/.ssh/id_ed25519
Your public key has been saved in /home/miner/.ssh/id_ed25519.pub
The key fingerprint is:
The key's randomart image is:
+--[ED25519 256]--+
|            +o .o|
|           ..ooo.|
|        o +ooo=o+|
|       .S= Bo.oBE||
|       .oo   =o o|
|      o.o ....  .|
+----[SHA256]-----+
```
