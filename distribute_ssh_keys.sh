#!/usr/bin/env bash
# Author: opselite@blockops.party 
# Date: 11/11/22
# Purpose: Distribute your ssh keys to miners found from the API.  
# Requirements: mmpOS API_KEY, FARM_ID, ssh access to the remote systems
# This script can be run on any system that has access to the remote systems

API_KEY=${1}
FARM_ID=${2}
API_URL="https://api.mmpos.eu/api/v1"
LIMIT=30
REMOTE_USER=miner

if [[ ! -f ~/.ssh/id_ed25519 ]]; then
  ssh-keygen -t ed25519 -C "miner@${HOST}"
fi

if [[ -z $API_KEY ]]; then
  echo "Invalid api_key or farm_id"
  echo "Usage: ${0} <API_KEY> <FARM_ID>"
  echo "Example: ${0} 123456-33-22-432-2342 343223-2342342-2342-2342"
  exit 1
fi

if [[ -z $FARM_ID ]]; then
  echo "Invalid api_key or farm_id"
  echo "Usage: ${0} <API_KEY> <FARM_ID>"
  echo "Example: ${0} 123456-33-22-432-2342 343223-2342342-2342-2342"
  exit 1
fi


RIGS=$(curl -s -H "X-API-Key: ${API_KEY}" ${API_URL}/${FARM_ID}/rigs\?limit\=$LIMIT | jq '.[].local_addresses[0]' | sort | uniq | tr -d \")

# A one time procedure to copy your ssh keys to all your rigs
# Ok if you accidently run this multiple times
function distribute_keys {
  for rig_ip in $RIGS; do
      echo "Working on ${rig_ip}"
      ssh-copy-id -o ConnectTimeout=5 -o StrictHostKeyChecking=no ${REMOTE_USER}@${rig_ip}
  done
}

distribute_keys
