#!/usr/bin/env python3
# Author: opselite@blockops.party
# Date: 11/11/22
# Purpose: Prints out a JSON formatted inventory list of all farms and rigs for use with ansible
# https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html?
# Requirements:
# 1. ansible
# 2. ~/.config/mmpos_config.yml file with below contents
# ---
# api_key: 1134i39219392399 <your key>
# limit: 30

import yaml
import requests
import json
import argparse
import os.path


def farms():
  conn_string = f'{api_url}/farms?limit={limit}'
  response = requests.get(conn_string, headers=headers)
  data = response.json()
  ids = map(lambda x: x['id'], data)

  return ids

def rigs(farm_id):
  conn_string = f'{api_url}/{farm_id}/rigs?limit={limit}'
  response = requests.get(conn_string, headers=headers)
  data = response.json()

  return data

def farm_ips(data):
  ips = map(lambda x: x['local_addresses'][0], data)

  return list(ips)

def children_info(data):
  return list(map(lambda x: {x['id']: {"hosts": [x['local_addresses'][0]]}}, data)),


def farm_data(farm_list):
  farm_data = {"all": {"children": []}}
  for farm_id in farm_list:
    api_data = rigs(farm_id)
    farm_data[farm_id] = {
      "hosts": farm_ips(api_data),
      "vars": {
        "ansible_ssh_user": "miner",
        "ansible_ssh_host_key_checking": False
        #"ansible_ssh_private_key_file": "~/.ssh/mykey",
      },
      #"children": children_info(api_data)
     
    }
    farm_data['all']['children'].append(farm_id)
  return farm_data

def load_config():
  config_data = {}
  config_file = os.path.expanduser('~/.config/mmpos_config.yml')
  with open(config_file) as file:
    try:
        config_data = yaml.safe_load(file)    
        
    except yaml.YAMLError as exc:
        print(exc)
  return config_data


if __name__ == '__main__':
    

    arg_parser = argparse.ArgumentParser(
        description=__doc__,
        prog=__file__
    )
    mandatory_options = arg_parser.add_mutually_exclusive_group()
    mandatory_options.add_argument(
        '--list',
        action='store',
        nargs="*",
        default="dummy",
        help="Show JSON of all managed hosts"
    )
    mandatory_options.add_argument(
        '--host',
        action='store',
        help="Display vars related to the host"
    )

    try:
        config_data = load_config()
        api_url = config_data.get('api_url', "https://api.mmpos.eu/api/v1")
        api_key = config_data.get('api_key')
        limit = config_data.get('limit', 30)
        headers = { 'X-API-Key': f"{api_key}"}


        args = arg_parser.parse_args()
        if args.host:
            print("{}")
        elif len(args.list) >= 0:         
            print(json.dumps(farm_data(farms()), indent=2, sort_keys=True))
        else:
            raise ValueError("Expecting either --host $HOSTNAME or --list")

    except ValueError:
        raise
