#!/usr/bin/env python3
MMPOS_API_URL = 'https://api.mmpos.eu'
MAX_THREAD_COUNT = 15

try:
    import click
    import os
    import json
    import urllib
    import threading
    from prettytable import PrettyTable
    from requests_cache import CachedSession

except ModuleNotFoundError:
    print("Supporting libraries were not installed, run pip3 install click prettytable requests_cache")
    exit(1)


@click.group()
def messages():
    pass


def call_api(path, params={"limit": 100}, data={}, method = 'GET'):
    session = CachedSession(cache_name='mmpos_cache', 
        backend='filesystem', 
        serializer='yaml',
        expire_after= 360, # 5 minutes
        allowable_methods=['GET']
    )
    headers = {
        'X-API-Key': os.environ['MMPOS_API_TOKEN'],
        'Content-Type': 'application/json'
    }
    url = urllib.parse.urljoin(MMPOS_API_URL, f'api/v1/{path}')
    try:
        if (method == 'GET'):
            response = session.get(url,params=params, headers=headers, data=json.dumps(data))
        elif (method == 'POST'):                      
            response = session.post(url,params=params, headers=headers, data=json.dumps(data))
        else:
        # method not supported
            raise Exception(f'method {method} is not supported')
    
        data = response.json()
        
    except json.decoder.JSONDecodeError:
        if (response.ok):
            data = response.content

    return data

rig_name_table = {}
def rig_name_list(refresh=False):
  if (len(rig_name_table) < 1 or refresh):
    for rig in all_rigs():
        rig_name_table[rig['id']] = rig['name']

  return rig_name_table  

def farms():
    farms = call_api('/farms', {}, {})
    return list(map(lambda x: x, farms))


def all_rigs():
    all_rigs = []
    for farm in farms():
        all_rigs.append(rigs(farm['id']))
    return flatten(all_rigs)


def flatten(x): return [i for row in x for i in row]


def rigs(farm_id):
    rigs = call_api(f'{farm_id}/rigs')
    list = []
    for rig in rigs:
        list.append(rig)
    return list

def default_farm():
    return farms()[0]

def set_rig_control(action, farm_id, rig_id):
    call_api(f'{farm_id}/rigs/{rig_id}/control', {}, {"control": action}, method='POST')
    click.echo(f'{rig_name_list()[rig_id]} has been set to {action}')
    return


@click.command()
def get_farms():
    t = PrettyTable()
    data = farms()
    t.field_names = data[0].keys()
    for farm in data:
        t.add_row(farm.values())

    click.echo(t)


@click.command()
@click.argument('farm_id', envvar='MMPOS_FARM_ID', default='first', type=click.STRING)
def get_rigs(farm_id):
    if (farm_id == 'first'):
        farm_id = farms()[0]['id']

    click.echo(rigs(farm_id))


@click.command()
def get_all_rigs():
    click.echo(all_rigs())


@click.command()
def gpu_table():
    t = PrettyTable()
    t.field_names = ['rig_name', 'name', 'address', 'gpu_id']

    rigs = all_rigs()
    for rig in rigs:
        for gpu in rig['gpus']:
            t.add_row([rig['name'], gpu['name'], rig['local_addresses']
                       [0], gpu['id']])

    click.echo(t)

def current_thread_count(items):
    active_threads = 0
    for item in items:
      if (item.is_alive()):
        active_threads = active_threads + 1

    return active_threads    

@click.command()
def rigs_table():
    t = PrettyTable()
    t.field_names = ['id', 'name', 'address', 'profiles', 'agent_version']

    rigs = all_rigs()
    for rig in rigs:
        profiles = list(
            map(lambda x: x['name'], rig['miner_profiles']))
        t.add_row([rig['id'], rig['name'], rig['local_addresses']
                  [0], profiles, rig['agent_version']])

    click.echo(t)

@click.command()
@click.option('--rig_id', type=click.STRING, required=True, help="The rig name or id, use all to run against all rigs in farm")
@click.option('--farm_id', type=click.STRING, default='first', show_default=True, help="The farm id, defaults to first farm found")
@click.option('--action', required=True,
              type=click.Choice(['disable', 'poweroff', 'reset', 'enable', 'restart', 'reboot'], case_sensitive=False))
def rig_control(action, rig_id, farm_id):
    if (farm_id == 'first'):
        farm_id = default_farm()['id']
    
    if (rig_id == 'all'):
        threads = []
        for rig in rigs(farm_id):
            x = threading.Thread(target=set_rig_control, args=(action.lower(), farm_id, rig['id']))
            if (current_thread_count(threads) > MAX_THREAD_COUNT):
                threads.pop(0).join() # wait for the first thread to finish

            x.start()
            threads.append(x) 
    else:        
        set_rig_control(action.lower(), farm_id, rig_id)


    click.echo('')

messages.add_command(rigs_table)
messages.add_command(get_all_rigs)
messages.add_command(get_farms)
messages.add_command(get_rigs)
messages.add_command(gpu_table)
messages.add_command(rig_control)
# @click.argument('token', envvar='MMPOS_API_TOKEN', type=click.STRING)

if __name__ == '__main__':
    if (not os.environ['MMPOS_API_TOKEN']):
        click.echo("You need to set the env variable MMPOS_API_TOKEN")
        exit(1)

    messages()
