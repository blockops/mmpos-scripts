#!/usr/bin/env bash
# Purpose: Runs the control action on all systems under control, uses the first farm found if multiple farms provided
# Date: 12/30/22
# Author: opselite@blockops.party

token=${2:-$MMPOS_API_TOKEN}
action=$1
api_url="https://api.mmpos.eu/api/v1"

if [[ -z $token ]]; then
  echo "Token not set, either set env variable MMPOS_API_TOKEN=your_api_token before running this script or pass as argument"
  echo "$0 <control action> <API_TOKEN> "
  echo "$0 poweroff 39232-2323232-232323232"
  exit 1
fi

if [[ -z $action ]]; then
 echo "Action not provided, must be one of: disable, enable, restart, reboot, reset, poweroff "
 echo "$0 <control action> <API_TOKEN> "
 echo "$0 poweroff 39232-2323232-232323232"
 exit 1
fi

echo $action |grep -q  '^disable\|^poweroff\|^reset\|^enable\|^restart\|^reboot' 

if [[ $? -ne 0 ]]; then
  echo "Invalid control action, must be one of: disable, enable, restart, reboot, reset, poweroff"
  exit 1
fi

farm_id=$(curl -H "X-API-Key: ${token}" ${api_url}/farms -s | jq ".[0].id" | tr -d \")
nodes=$(curl -H "X-API-Key: ${token}" "${api_url}/${farm_id}/rigs?limit=100" -s | jq ".[].id")

for node in $nodes; do
  n=$(echo $node | tr -d \")
  curl -X POST -H "X-API-Key: ${token}" "${api_url}/${farm_id}/rigs/${n}/control" -d "{\"control\": \"${action}\"}" -H "Content-type: application/json" &
  if [[ $? -eq 0 ]]; then
    echo "$n was set to ${action}"
  fi
done
wait
echo "All actions are now complete"

